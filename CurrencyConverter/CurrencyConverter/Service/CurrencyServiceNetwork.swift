//
//  CurrencyServiceNetwork.swift
//  CurrencyConverter
//
//  Created by Александр Пономарёв on 17/01/2019.
//  Copyright © 2019 Александр Пономарёв. All rights reserved.
//

import Alamofire
import Foundation

class CurrencyServiceNetwork: CurrencyService {
    func getCurrencies(_ completionHandler: @escaping (([Currency]) -> Void)) {
        request("http://data.fixer.io/api/latest?access_key=ea50066730ed2d2d16a9d0d8182d7dbb").responseData {
            switch $0.result {
            case let .success(data):
                let jsonDecoder = JSONDecoder()
                let rates = try? jsonDecoder.decode(Rates.self, from: data)
                let currencies = rates?.rates.map { keyValue -> Currency in
                    let (name, rate) = keyValue
                    return Currency(name: name, rate: rate)
                }
                completionHandler(currencies ?? [])
            case let .failure(error):
                print("Error: \(error)")
            }
        }
    }
}
