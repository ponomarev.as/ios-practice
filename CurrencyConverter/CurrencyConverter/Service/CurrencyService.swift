//
//  CurrencyService.swift
//  CurrencyConverter
//
//  Created by Александр Пономарёв on 17/01/2019.
//  Copyright © 2019 Александр Пономарёв. All rights reserved.
//

protocol CurrencyService {
    func getCurrencies(_ completionHandler: @escaping (([Currency]) -> Void))
}
