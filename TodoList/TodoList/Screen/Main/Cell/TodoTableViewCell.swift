//
//  TodoTableViewCell.swift
//  TodoList
//
//  Created by Александр Пономарёв on 12/01/2019.
//  Copyright © 2019 Александр Пономарёв. All rights reserved.
//

import UIKit

final class TodoTableViewCell: UITableViewCell {
    @IBOutlet private var messageLabel: UILabel!

    var todo: Todo? {
        didSet {
            messageLabel.text = todo?.message
        }
    }
}
