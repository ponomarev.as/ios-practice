//
//  Todo.swift
//  TodoList
//
//  Created by Александр Пономарёв on 12/01/2019.
//  Copyright © 2019 Александр Пономарёв. All rights reserved.
//

import Foundation
import RealmSwift

final class Todo: Object {
    @objc dynamic var message = ""
    @objc dynamic var createdAt = Date()
}
