//
//  AppDelegate.swift
//  TodoList
//
//  Created by Александр Пономарёв on 12/01/2019.
//  Copyright © 2019 Александр Пономарёв. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
